<?php

namespace SIWA\SiwaWebsocket\Utility;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitmqUtility
{
    /**
     * @var null|AMQPStreamConnection
     */
    protected $connection = null;

    /**
     * @var null|AMQPChannel
     */
    protected $channel = null;

    /**
     * RabbitmqUtility constructor.
     */
    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('rabbitmq', '5672', 'guest', 'guest', 'localhost');
        $this->channel = $this->connection->channel();
    }

    /**
     * @param string $message
     * @param string $topicName
     */
    public function publishMessage($message = '', $topicName = '')
    {
        $this->channel->exchange_declare($topicName, 'fanout', false, false, false);

        $msg = new AMQPMessage($message);
        $this->channel->basic_publish($msg, $topicName);
    }
}